# NginxConf-CounterTest


**in folder named counter-app :**
    1. Simple flask app that counts web site visits and stored in a default Redis backend
    2. Dockerfile
    3. pushed in docker hub in URL : (https://hub.docker.com/repository/docker/abdelrhman0110/counter-app1)    
     
**in Nginx-Test folder :**
    1. this a configuration of Nginx to marge be a front of this app
    2. Dockerfile
    3. pushed in docker hub in URL : (https://hub.docker.com/repository/docker/abdelrhman0110/nginx-test)   

**in docker-compose.yml file :**
    1. we marged all pushed files with redis to run together     
